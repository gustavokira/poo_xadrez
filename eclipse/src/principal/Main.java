package principal;

//imports para as classes do jar "poo_tabuleiro"
import br.sc.udesc.cct.dcc.tads.poo.tabuleiro.DesenharTabuleiro;
import br.sc.udesc.cct.dcc.tads.poo.tabuleiro.EstiloTabuleiro;
import br.sc.udesc.cct.dcc.tads.poo.tabuleiro.Peca;
import br.sc.udesc.cct.dcc.tads.poo.tabuleiro.Tabuleiro;

import modelo.Peao;
import modelo.Torre;

public class Main{
	public static void main(String[] args){
		
		Tabuleiro tabuleiro = new Tabuleiro(6,4);
		EstiloTabuleiro estilo = new EstiloTabuleiro();

		Peca pb1 = new Peao(Peca.BRANCA);
		Peca pb2 = new Peao(Peca.BRANCA);
		Peca pb3 = new Peao(Peca.BRANCA);
		Peca pb4 = new Peao(Peca.BRANCA);

		Peca tb1 = new Torre(Peca.BRANCA);
		Peca tb2 = new Torre(Peca.BRANCA);

		tabuleiro.addPeca(pb1, 1, 0);
		tabuleiro.addPeca(pb2, 1, 1);
		tabuleiro.addPeca(pb3, 1, 2);
		tabuleiro.addPeca(pb4, 1, 3);

		tabuleiro.addPeca(tb1, 0, 0);
		tabuleiro.addPeca(tb2, 0, 3);

		Peca pp1 = new Peao(Peca.PRETA);
		Peca pp2 = new Peao(Peca.PRETA);
		Peca pp3 = new Peao(Peca.PRETA);
		Peca pp4 = new Peao(Peca.PRETA);

		tabuleiro.addPeca(pp1, 4, 0);
		tabuleiro.addPeca(pp2, 4, 1);
		tabuleiro.addPeca(pp3, 4, 2);
		tabuleiro.addPeca(pp4, 4, 3);

		Peca tp1 = new Torre(Peca.PRETA);
		Peca tp2 = new Torre(Peca.PRETA);

		tabuleiro.addPeca(tp1, 5, 0);
		tabuleiro.addPeca(tp2, 5, 3);

		tp1.selecionar();
		
		tabuleiro.atualizar();
		
		//para este exercício, estas duas linhas não devem ser mexidas.
		DesenharTabuleiro saida = new DesenharTabuleiro();
		saida.desenhar(tabuleiro, estilo);
	}
}